package com.offcn.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.annotation.Resource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private AccessDeniedHandler accessDeniedHandler;


    //这个方法与用户名密码、角色相关
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    //页面改成我们的也页面

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable();
        //关闭csrf防护，跨站请求防护
        http.csrf().disable()
                //表单登录
        .formLogin()
                //登录页面
        .loginPage("/login.html")
                //登录访问路径，与页面表单提交路径要一致
        .loginProcessingUrl("/login")
                //登录成功访问的页面
        .defaultSuccessUrl("/index.html").permitAll()
                //登录失败的页面
        .failureUrl("/loginerror.html");
        //认证配置
        http.authorizeRequests()
                .antMatchers("/login.html","/login").permitAll()
                //配置静态页面可以访问
                .antMatchers("/loginstyle/**").permitAll()
                //任何请求
                .anyRequest()
                //验证与授权才能访问
                .authenticated();
        //配置无权限访问页面
//        http.exceptionHandling().accessDeniedPage("/pages/demo.html");
        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);
        //配置退出
        http.logout()
                //退出路径
                .logoutUrl("/logout")
                //退出后跳转页面
                .logoutSuccessUrl("/login.html");
    }
}
