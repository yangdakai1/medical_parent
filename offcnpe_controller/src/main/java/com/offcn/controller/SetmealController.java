package com.offcn.controller;


import com.offcn.pojo.Setmeal;
import com.offcn.pojo.SetmealAndCheckgroup;
import com.offcn.pojo.SetmealEcharsVo;
import com.offcn.service.SetmealService;
import com.offcn.utils.MessageConstant;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {


    @Reference //远程调用
    private SetmealService setmealService;

    @RequestMapping("/queryAllSetmeal")
    @PreAuthorize("hasAuthority('SETMEAL_QUERY')")
    public PageResult queryAllSetmeal(@RequestBody QueryPageBean queryPageBean){
        return setmealService.queryAllSetmeal(queryPageBean);
    }

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @PostMapping("/saveSetmeal")
    @PreAuthorize("hasAuthority('SETMEAL_ADD')")
    public Result saveSetmeal(@RequestBody Setmeal setmeal,int[] checkgroupIds){
        //成功保存到mysql之后再把这个图片名传到redis中
        SetOperations<String, String> opsForSet = redisTemplate.opsForSet();
        opsForSet.add("DB_SET",setmeal.getImg().replaceAll("/setmeal/",""));

        return setmealService.saveSetmeal(setmeal,checkgroupIds);
    }

    @GetMapping("/{id}")
    public Result getById(@PathVariable("id") int id){
        try {
            SetmealAndCheckgroup setmealAndCheckgroup = setmealService.getSetmealAndCheckgroup(id);
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS,setmealAndCheckgroup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
    }

    @RequestMapping("/deleteSetmeal")
    @PreAuthorize("hasAuthority('SETMEAL_DELETE')")
    public Result deleteSetmeal(int id){
        return setmealService.deleteSetmeal(id);
    }



    @RequestMapping("/updateSetmeal")
    @PreAuthorize("hasAuthority('SETMEAL_EDIT')")
    public Result updateSetmeal(@RequestBody Setmeal setmeal,Integer[] checkgroupIds){
        try {
            boolean result = setmealService.updateSetmealAndcheckgroup(setmeal, checkgroupIds);
            if (result){
                return new Result(true,MessageConstant.EDIT_SETMEAL_SUCCESS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(true,MessageConstant.EDIT_SETMEAL_FAIL);
    }

    @GetMapping("/countSetmeal")
    public Result countSetmeal(){
        try {
            List<SetmealEcharsVo> setmealEcharsVoList = setmealService.countSetmeal();
            return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,setmealEcharsVoList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);
    }

}

