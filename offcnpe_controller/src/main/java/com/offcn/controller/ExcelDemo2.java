package com.offcn.controller;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class ExcelDemo2 {
    public static void main(String[] args) throws IOException, InvalidFormatException {
        //创建文件对象
        File file =  new File("E:\\download\\ordersetting_template.xlsx");
        //把文件读取到一个工作簿
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        //获取工作表
        XSSFSheet sheet = workbook.getSheetAt(0);
        //获取多少行数据
        int rows = sheet.getPhysicalNumberOfRows();
        for (int i = 1;i<rows;i++){
            XSSFRow row = sheet.getRow(i);
            //获取单元格   一定要注意数据是要使用对应的类型
            Date v1 = row.getCell(0).getDateCellValue();
            double v2 = row.getCell(1).getNumericCellValue();
            System.out.println(v1);
            System.out.println(v2);;
        }

    }

}
