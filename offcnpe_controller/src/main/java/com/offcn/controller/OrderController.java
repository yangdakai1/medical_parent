package com.offcn.controller;


import com.offcn.pojo.Order;
import com.offcn.service.OrderService;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;

    @RequestMapping("/queryAllOrder")
    public PageResult queryAllOrder(@RequestBody QueryPageBean queryPageBean) {
        return orderService.queryAllOrder(queryPageBean);
    }

    @RequestMapping("/deleteOrder")
    public Result deleteOrder(int id){
        return orderService.deleteOrder(id);
    }

    @RequestMapping("/updateOrder")
    public Result updateOrder(@RequestBody Order order){
        return orderService.updateOrder(order);
    }
}

