package com.offcn.controller;


import com.offcn.pojo.Member;
import com.offcn.pojo.MemberEchartsVo;
import com.offcn.pojo.SetmealEcharsVo;
import com.offcn.service.MemberService;
import com.offcn.utils.*;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@RestController
@RequestMapping("/member")
public class MemberController {

    @Reference
    private MemberService memberService;
    @Value("${file.download.path}")
    private String path;

    @RequestMapping("/queryAllMember")
    public PageResult queryAllMember(@RequestBody QueryPageBean queryPageBean){
        return memberService.queryAllMember(queryPageBean);

    }

    @RequestMapping("/addMember")
    public Result addMember(@RequestBody Member member){
        return memberService.addMember(member);

    }
    @RequestMapping("/updateMember")
    public Result updateMember(@RequestBody Member member){
        return memberService.updateMember(member);

    }
    @RequestMapping("/deleteMember")
    public Result deleteMember(int id){
        return memberService.deleteMember(id);

    }

    @GetMapping("/downLoadExcel")
    public ResponseEntity<byte[]> downLoadExcel(QueryPageBean queryPageBean) throws IOException {
        //查数据库  list
        PageResult pageResult = memberService.listPage(queryPageBean);
        List<Member> list = pageResult.getRows();
        //生成excel  吧list的数据写成excel
        File file = listToExcel(list, queryPageBean.getCurrentPage());
        //下载excel
        return MyFileUtils.downLoadExcel(file);

    }

    private File listToExcel(List<Member> list,int pageNum) throws IOException {

        File file = new File(path+"会员数据第"+pageNum+"页.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("第" + pageNum + "页");
        //表头
        XSSFRow sheetRow = sheet.createRow(0);
        sheetRow.createCell(0).setCellValue("姓名");//name
        sheetRow.createCell(1).setCellValue("性别");//sex
        sheetRow.createCell(2).setCellValue("身份证号");//idcard
        sheetRow.createCell(3).setCellValue("电话号码");//phonenumber
        sheetRow.createCell(4).setCellValue("注册时间");//regTime
        sheetRow.createCell(5).setCellValue("邮箱");//email
        sheetRow.createCell(6).setCellValue("生日");//birthday
        int index=1;
        for (Member member : list) {
            XSSFRow row = sheet.createRow(index);
            row.createCell(0).setCellValue(member.getName());//name
            row.createCell(1).setCellValue(member.getSex());//sex
            row.createCell(2).setCellValue(member.getIdcard());//idcard
            row.createCell(3).setCellValue(member.getPhonenumber());//phonenumber
            row.createCell(4).setCellValue(member.getRegtime().toString());//regTime
            row.createCell(5).setCellValue(member.getEmail());//email
            row.createCell(6).setCellValue(member.getBirthday().toString());//birthday
            index++;
        }
        workbook.write(new FileOutputStream(file));
        return file;
    }

    @GetMapping("/countMemberBySex")
    public Result countMemberBySex(){
        try {
            List<MemberEchartsVo> memberEchartsVoList = memberService.countMemberBySex();
            return new Result(true, MessageConstant.QUERY_MEMBER_SUCCESS,memberEchartsVoList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,MessageConstant.QUERY_MEMBER_FAIL);
    }

    @GetMapping("/countMemberByRegTime")
    public Result countMemberByRegTime(){
        try {
            List<MemberEchartsVo> memberEchartsVoList = memberService.countMemberByRegTime();
            return new Result(true, MessageConstant.QUERY_MEMBER_SUCCESS,memberEchartsVoList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,MessageConstant.QUERY_MEMBER_FAIL);
    }

}

