package com.offcn.controller;

import com.offcn.service.BusinessService;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/business")
public class BusinessController {

    @Reference
    private BusinessService businessService;

    @GetMapping
    public Result getBusiness(){
        try {
            Map<String, Object> business = businessService.getBusiness();
            return new Result(true,"查询运营数据成功",business);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"查询运营数据失败");
        }
    }

}
