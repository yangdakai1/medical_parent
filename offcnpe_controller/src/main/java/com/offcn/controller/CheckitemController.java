package com.offcn.controller;


import com.offcn.pojo.Checkitem;
import com.offcn.service.CheckItemService;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@RestController
@RequestMapping("/checkitem")
public class CheckitemController {

    @Reference
    private CheckItemService checkItemService;

    @RequestMapping("/queryAllCheckItems")
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    public PageResult queryAllCheckItems(@RequestBody QueryPageBean queryPageBean){
        return checkItemService.getCheckItems(queryPageBean);
    }

    @RequestMapping("/addCheckItem")
    @PreAuthorize("hasAuthority('CHECKITEM_ADD')")
    public Result addCheckItem(@RequestBody Checkitem checkitem){
        return checkItemService.addCheckItem(checkitem);
    }
    @RequestMapping("/updateCheckItem")
    @PreAuthorize("hasAuthority('CHECKITEM_EDIT')")
    public Result updateCheckItem(@RequestBody Checkitem checkitem){
        return checkItemService.updateCheckItem(checkitem);
    }
    @RequestMapping("/deleteCheckItem")
    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")
    public Result deleteCheckItem(int id){
        return checkItemService.deleteCheckItem(id);
    }
    @RequestMapping("/getAllCheckItem")
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    public Result getAllCheckItem(){
        return checkItemService.getAllCheckItem();
    }

}

