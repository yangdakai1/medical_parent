package com.offcn.controller;


import com.offcn.pojo.Checkgroup;
import com.offcn.service.CheckGroupService;
import com.offcn.utils.MessageConstant;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@RestController
@RequestMapping("/checkgroup")
public class CheckgroupController {

    @Reference
    private CheckGroupService checkGroupService;


    @RequestMapping("/addCheckGroup")
    @PreAuthorize("hasAuthority('CHECKGROUP_ADD')")
    public Result addCheckGroup(@RequestBody Checkgroup checkgroup,int[] checkitemIds){

        return checkGroupService.addCheckGroup(checkgroup,checkitemIds);

    }

    @RequestMapping("/queryAllCheckGroups")
    @PreAuthorize("hasAuthority('CHECKGROUP_QUERY')")
    public PageResult queryAllCheckGroups(@RequestBody QueryPageBean queryPageBean){

        return checkGroupService.queryAllCheckGroups(queryPageBean);

    }
    @RequestMapping("/updateCheckGroup")
    @PreAuthorize("hasAuthority('CHECKGROUP_EDIT')")
    public Result updateCheckGroup(@RequestBody Checkgroup checkgroup,int[] checkitemIds){
        return checkGroupService.updateCheckGroup(checkgroup,checkitemIds);
    }

    @RequestMapping("/deleteCheckGroup")
    @PreAuthorize("hasAuthority('CHECKGROUP_DELETE')")
    public Result deleteCheckGroup(int id){
        return checkGroupService.deleteCheckGroup(id);
    }
    @RequestMapping("/getCheckItemIdByGroupId")
    @PreAuthorize("hasAuthority('CHECKGROUP_QUERY')")
    public List<Integer> getCheckItemIdByGroupId(int id){
        return checkGroupService.getCheckItemIdByGroupId(id);

    }

    @RequestMapping("/queryAllCheckGroup")
    @PreAuthorize("hasAuthority('CHECKGROUP_QUERY')")
    public Result queryAllCheckGroup(){
       return checkGroupService.queryAllCheckGroup();
    }
}

