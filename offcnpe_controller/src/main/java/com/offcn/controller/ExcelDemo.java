package com.offcn.controller;

import com.offcn.pojo.CheckgroupCheckitem;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ExcelDemo {
    public static void main(String[] args) throws Exception {
        List<CheckgroupCheckitem> list = new ArrayList<>();
        list.add(new CheckgroupCheckitem(89,56));
        list.add(new CheckgroupCheckitem(53,23));
        list.add(new CheckgroupCheckitem(54,87));
        //创建excel文件对象 空的
        File  file = new File("E:\\download\\dd.xlsx");
        //在内存中创建工作簿
        XSSFWorkbook workbook = new XSSFWorkbook();
        //创建工作表
        XSSFSheet sheet = workbook.createSheet("会员数据");
        //创建行
        XSSFRow row = sheet.createRow(0);
        //创建这一行的单元格
        row.createCell(0).setCellValue("checkgroupId");
        row.createCell(1).setCellValue("checkitemId");
        int index=1;
        //遍历
        for (CheckgroupCheckitem checkgroupCheckitem : list) {
            XSSFRow row1 = sheet.createRow(index);
            //创建这一行的单元格
            row1.createCell(0).setCellValue(checkgroupCheckitem.getCheckgroupId());
            row1.createCell(1).setCellValue(checkgroupCheckitem.getCheckitemId());
            index++;
        }
        //把内存的数据写到Excel
        workbook.write(new FileOutputStream(file));
        System.out.println("yes");
    }
}
