package com.offcn.controller;


import com.offcn.pojo.CalenderVo;
import com.offcn.pojo.Ordersetting;
import com.offcn.service.OrdersettingService;
import com.offcn.utils.LocalDateUtils;
import com.offcn.utils.MessageConstant;
import com.offcn.utils.MyFileUtils;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@RestController
@RequestMapping("/ordersetting")
public class OrdersettingController {

    @Value("${file.download.path}")
    private String path;

    @Reference
    private OrdersettingService ordersettingService;


    @RequestMapping("/uploadExcel")
    public Result uploadExcel(@RequestParam("excelFile") MultipartFile multipartFile){
        //执行上传    2021/11/05/202111050948238987.xlsx
        String filepath = MyFileUtils.upload(multipartFile, path);
        if(filepath==null){//上传失败
            return new Result(false, MessageConstant.UPLOAD_FAIL);
        }
        //把Excel读取List
        try {
            List<Ordersetting> ordersettings = excelToList(filepath);
            if(ordersettings==null || ordersettings.size()==0){//excel没有写数据
                return new Result(false, "execl没有填写数据,请检Excel数据格式是否正确！");
            }
            //添加数据
            ordersettingService.saveList(ordersettings);
            return new Result(true,"预约设置成功！");
        } catch (IOException | InvalidFormatException | IllegalStateException e) {//excel的数据有问题
            e.printStackTrace();
            return new Result(false, "数据读取失败,请检Excel数据格式是否正确！");
        } catch(Exception e){//添加mysql有错
            e.printStackTrace();
            return new Result(false, "数据添加失败，请稍后再试！");
        }finally {
            File file=new File(path+filepath);
            //文件读取完了，没有用了
            file.delete();
        }
    }

    private List<Ordersetting> excelToList(String filePath) throws IOException, InvalidFormatException {
        List<Ordersetting> list = new ArrayList<>();
        File file = new File(path+filePath);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        int rows = sheet.getPhysicalNumberOfRows();
        for (int i =1;i<rows;i++){
            XSSFRow row = sheet.getRow(i);
            Date date = row.getCell(0).getDateCellValue();
            LocalDate localDate = LocalDateUtils.date2LocalDate(date);
            double num = row.getCell(1).getNumericCellValue();
            int intValue = new Double(num).intValue();
            Ordersetting ordersetting = new Ordersetting(localDate,intValue);
            list.add(ordersetting);

        }

        workbook.close();
        return list;
    }

    @GetMapping    //localhost:9001/ordersetting?time=2021/11
    public Result list(String time){//     2021/11
        try {
            List<CalenderVo> list = ordersettingService.list(time);
            return new Result(true,MessageConstant.QUERY_ORDER_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ORDER_FAIL);
        }
    }

    @RequestMapping("/updateOrder")
    public Result updateOrder(String time,int number){
        try {
            return ordersettingService.updateOrder(time,number);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,"修改失败！");
    }

}

