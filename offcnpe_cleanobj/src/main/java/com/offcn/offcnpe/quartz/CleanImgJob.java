package com.offcn.offcnpe.quartz;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.util.Set;

@Component
public class CleanImgJob {

    @Value("${file.upload.path}")
    private String path;

    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @Scheduled(cron = "0 27 15 ? * *")
    public void clean(){
        SetOperations<String,String> opsForSet = redisTemplate.opsForSet();
        Set<String> difference = opsForSet.difference("UPLOAD_SET", "DB_SET");
        if (difference!=null && difference.size()>0){
            for (String pic : difference) {
                File file = new File(path+pic);
                if (file.exists()){
                    file.delete();
                }
            }
        }
        redisTemplate.delete("UPLOAD_SET");
        redisTemplate.delete("DB_SET");

        System.out.println("clean img success...");
    }
}
