package com.offcn.offcnpe.quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class MyScheduler {
    public static void main(String[] args) throws SchedulerException {
        //创建一个jobDetail的实例，将该实例域MyJob class绑定
        JobDetail jobDetail = JobBuilder.newJob(MyJob.class).withIdentity("myJob").build();
        //创建一个Trigger触发器的实例，定义该job立即执行，并且每2秒执行一次，一直执行
//        SimpleTrigger trigger = TriggerBuilder.newTrigger()
//                .withIdentity("myTrigger")//名称，自定义
//                .startNow() //现在就开始执行
//                .withSchedule(
//                        //setTimeout n秒后执行一次
//                        //setInterval 每n秒执行一次
//                        SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(2)//每2秒执行一次
//                        .repeatForever()//永远重复
//
//                ).build();

        //0 51 10 * * ?   每天早上10：51执行
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("cronTrigger").withSchedule(CronScheduleBuilder.cronSchedule("0 51 10 * * ? ")).build();
        //创建schedule实例
        StdSchedulerFactory factory = new StdSchedulerFactory();
        //调度器
        Scheduler scheduler = factory.getScheduler();
        scheduler.start();
        //调度器执行，做什么事情，什么时候做
        scheduler.scheduleJob(jobDetail,cronTrigger);
    }
}
