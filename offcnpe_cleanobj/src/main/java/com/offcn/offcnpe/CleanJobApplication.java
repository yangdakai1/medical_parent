package com.offcn.offcnpe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableScheduling //开启定时任务
public class CleanJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(CleanJobApplication.class,args);
    }
}
