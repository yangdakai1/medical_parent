package com.offcn.service;

import com.offcn.pojo.Member;
import com.offcn.pojo.MemberEchartsVo;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;

import java.util.List;

public interface MemberService {
    PageResult queryAllMember(QueryPageBean queryPageBean);

    Result addMember(Member member);

    Result updateMember(Member member);

    Result deleteMember(int id);

    PageResult listPage(QueryPageBean queryPageBean);

    List<MemberEchartsVo> countMemberBySex();

    List<MemberEchartsVo> countMemberByRegTime();

    int getTodayNewMember();//本日新增会员数
    int getTotalMember();//总会员数
    int getThisWeekNewMember();//本周新增会员数
    int getThisMonthNewMember();//本月新增会员数
}
