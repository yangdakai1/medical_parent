package com.offcn.service;

import com.offcn.pojo.Permission;

import java.util.List;

public interface PermissionService {
    List<Permission> getPermissionById(int id);
}
