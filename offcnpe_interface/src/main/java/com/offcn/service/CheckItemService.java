package com.offcn.service;

import com.offcn.pojo.Checkitem;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;

public interface CheckItemService {

    public PageResult getCheckItems(QueryPageBean queryPageBean);
    public Result addCheckItem(Checkitem checkitem);
    public Result updateCheckItem(Checkitem checkitem);
    public Result deleteCheckItem(int id);

    Result getAllCheckItem();
}
