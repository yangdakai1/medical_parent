package com.offcn.service;

import com.offcn.pojo.Order;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;

public interface OrderService {
    int getTodayOrderNumber();//今日预约数
    int getTodayVisitsNumber();//今日到诊数
    int getThisWeekOrderNumber();//本周预约数
    int getThisWeekVisitsNumber();//本周到诊数
    int getThisMonthOrderNumber();//本月预约数
    int getThisMonthVisitsNumber();//本月到诊数

    PageResult queryAllOrder(QueryPageBean queryPageBean);

    Result deleteOrder(int id);

    Result updateOrder(Order order);
}
