package com.offcn.service;

import com.offcn.pojo.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> getMenuById(int id);
}
