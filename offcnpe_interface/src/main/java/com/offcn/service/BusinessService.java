package com.offcn.service;

import java.util.Map;

public interface BusinessService {
    Map<String,Object> getBusiness() throws Exception;
}
