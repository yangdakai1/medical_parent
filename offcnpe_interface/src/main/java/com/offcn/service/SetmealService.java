package com.offcn.service;

import com.offcn.pojo.Setmeal;
import com.offcn.pojo.SetmealAndCheckgroup;
import com.offcn.pojo.SetmealEcharsVo;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;

import java.util.List;
import java.util.Map;

public interface SetmealService {
    PageResult queryAllSetmeal(QueryPageBean queryPageBean);

    Result saveSetmeal(Setmeal setmeal, int[] checkgroupIds);

    //通过id查询套餐信息
    SetmealAndCheckgroup getSetmealAndCheckgroup(int id);

    Result deleteSetmeal(int id);

    boolean updateSetmealAndcheckgroup(Setmeal setmeal, Integer[] checkgroupIds);

    List<SetmealEcharsVo> countSetmeal();

    List<Setmeal> quertListSetmeal();

    Setmeal getSetmealById(int id);

    Setmeal getInfoById(int id);

    Result setOrder(Map<String,String> map);
}
