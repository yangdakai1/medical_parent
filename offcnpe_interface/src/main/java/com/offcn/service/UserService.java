package com.offcn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.offcn.pojo.User;

public interface UserService extends IService<User> {
    User getUserByName(String account);
}
