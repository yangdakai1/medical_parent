package com.offcn.service;

import com.offcn.pojo.Checkgroup;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;

import java.util.List;

public interface CheckGroupService {

    Result addCheckGroup(Checkgroup checkgroup, int[] checkitemIds);

    PageResult queryAllCheckGroups(QueryPageBean queryPageBean);

    Result updateCheckGroup(Checkgroup checkgroup, int[] checkitemIds);

    Result deleteCheckGroup(int id);

    List<Integer> getCheckItemIdByGroupId(int id);

    Result queryAllCheckGroup();
}
