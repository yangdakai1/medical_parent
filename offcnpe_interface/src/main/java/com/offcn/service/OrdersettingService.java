package com.offcn.service;

import com.offcn.pojo.CalenderVo;
import com.offcn.pojo.Ordersetting;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;

import java.util.List;

public interface OrdersettingService {

    boolean saveList(List<Ordersetting> list);

    List<CalenderVo> list(String yearAndMonth);

    Result updateOrder(String time,int number);


}
