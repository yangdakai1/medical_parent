package com.offcn.service;

import com.offcn.pojo.Role;

import java.util.List;

public interface RoleService {

    List<Role> listByRoleId(int id);
}
