
医疗管家项目介绍
该项目是前后端分离项目，实现了从数据库表展示检查项,检查组，套餐、会员等信息，设置可预约时间，前台提交套餐预约。
预约管理设置了一些条件
1.对用户所选日期是否进行了预约设置，入如果没有设置则无法进行预约。
2.检查用户所选择的预约日期是否已经约满，如果约满则无法进行预约
3、检查用户是否重复预约《同一个用户在同一天预约了同一个套餐)，如果是重复预约则无法完成再次预约
4、检查当前用户是否为会员，如果是会员则直接完成预约，如果不是会员则自动完成注册并进行预约
5、预约成功，更新当日的已预约人数
定时清理垃圾图片功能，还有登录控制，权限管理，文件的上传下载。

软件架构
基于springboot的微服务开发的
jdk1.8
maven3.5.4
dubbo远程调用
zookeeper做负载均衡
redis做缓存

前端使用vue+elementui
使用axios异步请求响应数据


安装教程
使用git软件git clone 仓库地址 拉取代码



页面展示
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/160949_0eef6211_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161022_c37d7cb1_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161036_bbc5a8a1_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161045_c2c869d5_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161057_5ed3c5ef_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161116_14fb8c49_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161124_80cd054f_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161131_c6ca71b0_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161140_3bae266b_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161153_256d098b_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161200_95dbb7fa_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161745_7c7a4c16_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161754_1f39c826_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161804_38aed345_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161812_2a21b0a7_8968971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1211/161928_25191b17_8968971.png "屏幕截图.png")