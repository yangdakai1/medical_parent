package com.offcn.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_checkgroup_checkitem")
@AllArgsConstructor
@NoArgsConstructor
public class CheckgroupCheckitem extends Model {

    private static final long serialVersionUID = 1L;

      private Integer checkgroupId;

    private Integer checkitemId;


}
