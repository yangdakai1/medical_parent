package com.offcn.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalenderVo implements Serializable {

    private Integer date;
    private Integer number;
    private Integer  reservations;
}
