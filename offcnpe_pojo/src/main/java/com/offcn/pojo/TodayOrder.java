package com.offcn.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TodayOrder implements Serializable {
    private String setmealName;
    private String memberName;
    private Integer phoneNumber;
    private Date orderDate;
    private String orderType;
    private String orderStatus;


}
