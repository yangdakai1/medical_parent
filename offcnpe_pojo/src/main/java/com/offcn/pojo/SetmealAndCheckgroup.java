package com.offcn.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetmealAndCheckgroup implements Serializable {
    private Setmeal setmeal; //当前套餐信息
    private List<Integer> checkgroupIds;//这个套餐有什么检查组
}
