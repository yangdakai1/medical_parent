package com.offcn.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class LocalDateUtils {
    /**
     * LocalDate转Date
     * @param localDate
     * @return
     */
    public static Date localDate2Date(LocalDate localDate) {
        if (null == localDate) {
            return null;
        }
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * Date转LocalDate
     * @param date
     */
    public static LocalDate date2LocalDate(Date date) {
        if(null == date) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * String转LocalDate
     * @param time
     * @return
     */

    public static LocalDate string2LocalDate(String time){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date  date = null;
        try {
            date = formatter.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter1.format(date);
        LocalDate startDate = LocalDate.parse(format, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return startDate;
    }

}
