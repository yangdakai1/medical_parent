package com.offcn.utils;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyFileUtils {

    /**
     * 上传文件，并返回上传成功之后的完整文件名称
     * @param multipartFile  上传文件对象
     * @param path    必传，上传的路径   F:/upload/
     * @return   完整文件名称：2021/11/3/pic_202111031434120832.jpg
     */
    public static String upload(MultipartFile multipartFile, String path){
        String timePath = mkdir(path);
        //获取文件名
        String filename = multipartFile.getOriginalFilename();//这是整个文件的名称
        String newName = createName(filename);
        File file=new File(path+timePath+newName);//这个是空的   F:/upload/2021/11/3/pic_202111031434120832.jpg
        //把内存中的数据写到file中去
        try {
            multipartFile.transferTo(file);
            return timePath+newName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //拓展---按时间来对象进行分类，创建对应的目录
    private static String mkdir(String path){
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy/MM/dd");
        String timePath = dateFormat.format(new Date())+"/";//     2021/11/3/
        path=path+timePath;//F:/upload/2021/11/3
        File file=new File(path);//创建当前目录对象
        if(!file.exists()){//这个目录不存在
            file.mkdirs();//允许创建多层目录
        }
        return timePath;//   2021/11/3/
    }

    //重命名
    private static String createName(String filename){
        //获取后缀名
        String suffixName = filename.substring(filename.lastIndexOf("."));
        //pic_2021110314073489790.jpg
        //获取字时间字符串
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
        String timeString = dateFormat.format(new Date());
        //生成4位随机数
        String randomString = String.valueOf(Math.random()).substring(2, 6);
        //接拼新的名字
        String newName="pic_"+timeString+randomString+suffixName;
        return newName;
    }
    //文件下载
    public static ResponseEntity<byte[]> downLoadExcel(File file) throws IOException {

        //把文件读取到字节数组
        byte[] responseBody = FileUtils.readFileToByteArray(file);
        //头部信息
        HttpHeaders headers = new HttpHeaders();
        //设置响应类型   二进制的方式，代表可以下载所有的文件类型
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        //设置弹框 设置文件名  浏览器不要utf-8  要ios-8859-1
        String newName = new String(file.getName().getBytes(StandardCharsets.UTF_8), "ISO_8859_1");
        headers.setContentDispositionFormData("attachment",newName);
        //响应体内容，头部信息，状态码
        return new ResponseEntity<>(responseBody,headers, HttpStatus.OK);
    }
}