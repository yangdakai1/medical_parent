package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.OrdersettingMapper;
import com.offcn.pojo.CalenderVo;
import com.offcn.pojo.Ordersetting;
import com.offcn.pojo.TodayOrder;
import com.offcn.service.OrdersettingService;
import com.offcn.utils.LocalDateUtils;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrdersettingServiceImpl implements OrdersettingService {

    @Resource
    private OrdersettingMapper ordersettingMapper;


    @Override
    public boolean saveList(List<Ordersetting> list) {
        for (Ordersetting ordersetting : list) {
            QueryWrapper<Ordersetting> queryWrapper = new QueryWrapper<>();
            //判这个日期是是已经存在
            queryWrapper.eq("orderDate",ordersetting.getOrderdate());
            List<Ordersetting> selectList = ordersettingMapper.selectList(queryWrapper);
            if (selectList!=null && selectList.size()>0){//已经有了这个日期，修改
                Ordersetting dbordersetting = selectList.get(0);
                if (ordersetting.getNumber()>dbordersetting.getReservations()){//number必须大于已经预约人数
                    dbordersetting.setNumber(ordersetting.getNumber());
                }
                ordersettingMapper.updateById(ordersetting);
            }else{
                ordersettingMapper.insert(ordersetting);
            }
        }
        return true;
    }

    @Override
    public List<CalenderVo> list(String yearAndMonth) {
        List<CalenderVo> list = new ArrayList<>();
        String beginTime = yearAndMonth+"/01";
        String endTime = yearAndMonth+"/31";
        QueryWrapper<Ordersetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.between("orderDate",beginTime,endTime);
        List<Ordersetting> ordersettingList = ordersettingMapper.selectList(queryWrapper);
        if (ordersettingList!=null && ordersettingList.size()>0){
            for (Ordersetting ordersetting : ordersettingList) {
                int day = ordersetting.getOrderdate().getDayOfMonth();
                list.add(new CalenderVo(day,ordersetting.getNumber(),ordersetting.getReservations()));
            }
        }
        return list;
    }

    @Override
    public Result updateOrder(String time, int number) {
        //判断这个时间设置了没有
        QueryWrapper<Ordersetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orderDate",time);
        List<Ordersetting> ordersettingList = ordersettingMapper.selectList(queryWrapper);
        if (ordersettingList!=null && ordersettingList.size()>0){
            Ordersetting ordersetting = ordersettingList.get(0);
            Integer reservations = ordersetting.getReservations();
            if (number<reservations){
                return  new Result(false,"该日期已经预约了"+reservations+"人，新设置的人数不少于"+reservations+"人");
            }
            ordersetting.setNumber(number);
            ordersettingMapper.updateById(ordersetting);
            return new Result(true,"成功修改可预约人数");
        }
        //添加
        LocalDate localDate = LocalDateUtils.string2LocalDate(time);
        Ordersetting ordersetting = new Ordersetting(0, localDate, number, 0);
        ordersettingMapper.insert(ordersetting);
        return new Result(true,"成功设置可预约人数");
    }

}
