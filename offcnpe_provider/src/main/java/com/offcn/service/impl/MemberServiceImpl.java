package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.MemberMapper;
import com.offcn.mapper.OrderMapper;
import com.offcn.pojo.Member;
import com.offcn.pojo.MemberEchartsVo;
import com.offcn.pojo.Order;
import com.offcn.service.MemberService;
import com.offcn.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {

    @Resource
    private MemberMapper memberMapper;
    @Resource
    private OrderMapper orderMapper;

    @Override
    public PageResult queryAllMember(QueryPageBean queryPageBean) {
        Page<Member> page = new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        String queryString = queryPageBean.getQueryString();
        String sex = queryPageBean.getSex();
        String regTime = queryPageBean.getReg();
        String age = queryPageBean.getAge();

        int zero=0;
        int eighteen=0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        String nowTime = dateFormat.format(new Date());
        int time = Integer.parseInt(nowTime);
        if (age.contains("-")){
            String[] split = age.split("-");

            int perAge = Integer.parseInt(split[0]);
            int nextAge = Integer.parseInt(split[1]);

            zero = time-perAge;
            eighteen = time-nextAge;
        }


        if (StringUtils.isNotBlank(queryString) && !sex.equals("不限") && !regTime.equals("不限") && !age.equals("不限")){
            queryWrapper.eq("sex",sex)
                    .between("birthday", eighteen +"-01-01", zero +"-01-01")
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString))
                    .and(reg -> reg.like("regTime",regTime+"%"));
        }else if (StringUtils.isNotBlank(queryString) && !sex.equals("不限")){
            queryWrapper.eq("sex",sex)
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString));
        }else if (StringUtils.isNotBlank(queryString) && !regTime.equals("不限")){
            queryWrapper.like("regTime",regTime+"%")
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString));
        }else if (!sex.equals("不限") && !regTime.equals("不限")){
            queryWrapper.eq("sex",sex)
                    .and(reg -> reg.like("regTime",regTime+"%"));
        }else if (StringUtils.isNotBlank(queryString)  && !age.equals("不限")){
            queryWrapper
                    .between("birthday",eighteen+"-01-01",zero+"-01-01")
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString));

        }else if (!sex.equals("不限")  && !age.equals("不限")){
            queryWrapper.eq("sex",sex)
                    .between("birthday",eighteen+"-01-01",zero+"-01-01");

        }else if (!regTime.equals("不限")  && !age.equals("不限")) {
            queryWrapper
                    .between("birthday", eighteen + "-01-01", zero + "-01-01")
                    .and(reg -> reg.like("regTime", regTime + "%"));

        }
        else if (!sex.equals("不限")){
            queryWrapper.eq("sex",sex);

        }else if (!regTime.equals("不限")){
            queryWrapper.like("regTime",regTime+"%");

        }else if (StringUtils.isNotBlank(queryString)){
            queryWrapper.like("name",queryString)
                    .or().like("phoneNumber",queryString);

        }else if (!age.equals("不限")){
            queryWrapper.between("birthday",eighteen+"-01-01",zero+"-01-01");
        }
        if (StringUtils.isNotBlank(queryString) && !sex.equals("不限") && !regTime.equals("不限")){
            queryWrapper.eq("sex",sex)
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString))
                    .and(reg -> reg.like("regTime",regTime+"%"));
        }

        if (!sex.equals("不限") && !age.equals("不限") && !regTime.equals("不限")) {
            queryWrapper.eq("sex",sex)
                    .between("birthday", eighteen +"-01-01", zero +"-01-01")
                    .and(reg -> reg.like("regTime",regTime+"%"));

        }
        if (StringUtils.isNotBlank(queryString) && !regTime.equals("不限")  && !age.equals("不限")) {
            queryWrapper
                    .between("birthday", eighteen +"-01-01", zero +"-01-01")
                    .and(reg -> reg.like("regTime",regTime+"%"))
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString));;

        }
        if (StringUtils.isNotBlank(queryString) && !sex.equals("不限")  && !age.equals("不限")) {
            queryWrapper.eq("sex",sex)
                    .between("birthday", eighteen +"-01-01", zero +"-01-01")
                    .and(name -> name.like("name",queryString).or().like("phoneNumber",queryString));

        }
        Page<Member> memberPage = memberMapper.selectPage(page, queryWrapper);
        PageResult pageResult = new PageResult(memberPage.getTotal(), memberPage.getRecords());
        return pageResult;

    }

    @Override
    public PageResult listPage(QueryPageBean queryPageBean) {
        QueryWrapper<Member> queryWrapper=new QueryWrapper<>();
        String queryString = queryPageBean.getQueryString();
        if(queryString!=null && queryString.length()>0 && !queryString.equals("null")){
            queryWrapper.or().like("phoneNumber",queryString)
                    .or().like("name",queryString);
        }
        Page page = new Page(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page dbPage = memberMapper.selectPage(page, queryWrapper);
        return new PageResult(dbPage.getTotal(),dbPage.getRecords());
    }

    @Override
    public Result addMember(Member member) {
        int rows = memberMapper.insert(member);
        if (rows==1){
            return new Result(true, MessageConstant.ADD_MEMBER_SUCCESS);
        }
        return new Result(false,MessageConstant.ADD_MEMBER_FAIL);
    }

    @Override
    public Result updateMember(Member member) {
        int rows = memberMapper.updateById(member);
        if (rows==1){
            return new Result(true, MessageConstant.EDIT_MEMBER_SUCCESS);
        }
        return new Result(false,MessageConstant.EDIT_MEMBER_FAIL);
    }

    @Override
    public Result deleteMember(int id) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("member_id",id);
        orderMapper.delete(queryWrapper);
        int rows = memberMapper.deleteById(id);
        if (rows==1){
            return new Result(true, MessageConstant.DELETE_MEMBER_SUCCESS);
        }
        return new Result(false,MessageConstant.DELETE_MEMBER_FAIL);
    }

    @Override
    public List<MemberEchartsVo> countMemberBySex() {
        return memberMapper.countMemberBySex();
    }

    @Override
    public List<MemberEchartsVo> countMemberByRegTime() {
        return memberMapper.countMemberByRegTime();
    }

    @Override
    public int getTodayNewMember() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("regTime",today);
            return memberMapper.selectCount(queryWrapper);

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getTotalMember() {
        return memberMapper.selectCount(null);
    }

    @Override
    public int getThisWeekNewMember() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            String monday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
            QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
            queryWrapper.between("regTime",monday,today);
            return memberMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getThisMonthNewMember() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            String month = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());
            QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
            queryWrapper.between("regTime",month,today);
            return memberMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
