package com.offcn.service.impl;

import com.offcn.mapper.SetmealMapper;
import com.offcn.service.BusinessService;
import com.offcn.service.MemberService;
import com.offcn.service.OrderService;
import com.offcn.utils.DateUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class BusinessServiceImpl implements BusinessService {

    @Resource
    private OrderService orderService;

    @Resource
    private MemberService memberService;

    @Resource
    private SetmealMapper setmealMapper;

    @Override
    public Map<String, Object> getBusiness() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("reportDate", DateUtils.parseDate2String(new Date()));
        data.put("todayNewMember",memberService.getTodayNewMember());
        data.put("totalMember",memberService.getTotalMember());
        data.put("thisWeekNewMember",memberService.getThisWeekNewMember());
        data.put("thisMonthNewMember",memberService.getThisMonthNewMember());
        data.put("todayOrderNumber",orderService.getTodayOrderNumber());
        data.put("todayVisitsNumber",orderService.getTodayVisitsNumber());
        data.put("thisWeekOrderNumber",orderService.getThisWeekOrderNumber());
        data.put("thisWeekVisitsNumber",orderService.getThisWeekVisitsNumber());
        data.put("thisMonthOrderNumber",orderService.getThisMonthOrderNumber());
        data.put("thisMonthVisitsNumber",orderService.getThisMonthVisitsNumber());
        data.put("hotSetmeal",setmealMapper.listHotSetmeal());//[]
        return data;
    }
}
