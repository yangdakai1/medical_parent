package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.CheckgroupCheckitemMapper;
import com.offcn.mapper.CheckgroupMapper;
import com.offcn.mapper.SetmealCheckgroupMapper;
import com.offcn.pojo.Checkgroup;
import com.offcn.pojo.CheckgroupCheckitem;
import com.offcn.pojo.SetmealCheckgroup;
import com.offcn.service.CheckGroupService;
import com.offcn.utils.MessageConstant;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CheckGroupServiceImpl implements CheckGroupService {

    @Resource
    private CheckgroupMapper checkgroupMapper;

    @Resource
    private CheckgroupCheckitemMapper checkgroupCheckitemMapper;

    @Resource
    private SetmealCheckgroupMapper setmealCheckgroupMapper;
    @Override
    public Result addCheckGroup(Checkgroup checkgroup, int[] checkitemIds) {

        int rows = checkgroupMapper.insert(checkgroup);
        if (checkitemIds!=null && checkitemIds.length>0){
            for (int checkitemId : checkitemIds) {
                CheckgroupCheckitem checkgroupCheckitem = new CheckgroupCheckitem();
                checkgroupCheckitem.setCheckgroupId(checkgroup.getId());
                checkgroupCheckitem.setCheckitemId(checkitemId);
                checkgroupCheckitemMapper.insert(checkgroupCheckitem);
            }
        }
        if (rows==1){
            return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
        }
        return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);

    }

    @Override
    public PageResult queryAllCheckGroups(QueryPageBean queryPageBean) {
        Page<Checkgroup> page = new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        QueryWrapper<Checkgroup> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(queryPageBean.getQueryString())) {
            queryWrapper.like("code", queryPageBean.getQueryString())
                    .or().like("name", queryPageBean.getQueryString())
                    .or().like("helpCode", queryPageBean.getQueryString());
        }
        Page<Checkgroup> checkgroupPage = checkgroupMapper.selectPage(page, queryWrapper);
        PageResult pageResult = new PageResult(checkgroupPage.getTotal(),checkgroupPage.getRecords());
        return pageResult;
    }

    @Override
    public Result updateCheckGroup(Checkgroup checkgroup, int[] checkitemIds) {
        //更新检查组数据
        int rows = checkgroupMapper.updateById(checkgroup);
        //删除检查组和检查项的中间关系表数据
        Map<String,Object> map = new HashMap<>();
        map.put("checkgroup_id",checkgroup.getId());
        checkgroupCheckitemMapper.deleteByMap(map);
        //从新添加检查组和检查项的中间表数据
        for(Integer id:checkitemIds){
            CheckgroupCheckitem checkgroupCheckitem = new CheckgroupCheckitem();
            checkgroupCheckitem.setCheckgroupId(checkgroup.getId());
            checkgroupCheckitem.setCheckitemId(id);
            checkgroupCheckitemMapper.insert(checkgroupCheckitem);
        }
        if (rows==1){
            return new Result(true,MessageConstant.EDIT_CHECKGROUP_SUCCESS);
        }
        return  new Result(false, MessageConstant.EDIT_CHECKGROUP_FAIL);
    }

    @Override
    public Result deleteCheckGroup(int id) {
        QueryWrapper<CheckgroupCheckitem> queryWrapper= new QueryWrapper<>();
        queryWrapper.eq("checkgroup_id",id);
        checkgroupCheckitemMapper.delete(queryWrapper);

        QueryWrapper<SetmealCheckgroup> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("checkgroup_id",id);
        setmealCheckgroupMapper.delete(queryWrapper1);

        int rows = checkgroupMapper.deleteById(id);
        if (rows==1){
            return new Result(true, MessageConstant.DELETE_CHECKGROUP_SUCCESS);
        }
        return new Result(false, MessageConstant.DELETE_CHECKGROUP_FAIL);
    }

    @Override
    public List<Integer>  getCheckItemIdByGroupId(int id) {
        LambdaQueryWrapper<CheckgroupCheckitem> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.eq(CheckgroupCheckitem::getCheckgroupId,id);
        lambdaQueryWrapper.select(CheckgroupCheckitem::getCheckitemId);

        return checkgroupCheckitemMapper.selectObjs(lambdaQueryWrapper).stream().map(o -> (Integer) o)
                .collect(Collectors.toList());
    }

    @Override
    public Result queryAllCheckGroup() {
        List<Checkgroup> checkgroups=null;
        try {
            checkgroups = checkgroupMapper.selectList(null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"获取检查组失败");
        }
        return new Result(true,"获取检查组成功",checkgroups);
    }
}
