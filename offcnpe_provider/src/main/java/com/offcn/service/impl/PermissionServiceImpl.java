package com.offcn.service.impl;

import com.offcn.mapper.PermissionMapper;
import com.offcn.pojo.Permission;
import com.offcn.service.PermissionService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> getPermissionById(int id) {
        return permissionMapper.getPermissionById(id);
    }
}
