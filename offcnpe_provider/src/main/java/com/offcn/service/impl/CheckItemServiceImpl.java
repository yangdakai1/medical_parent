package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.CheckgroupCheckitemMapper;
import com.offcn.mapper.CheckitemMapper;
import com.offcn.pojo.CheckgroupCheckitem;
import com.offcn.pojo.Checkitem;
import com.offcn.pojo.TodayOrder;
import com.offcn.service.CheckItemService;
import com.offcn.utils.MessageConstant;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CheckItemServiceImpl implements CheckItemService {
    @Resource
    private CheckitemMapper checkitemMapper;
    @Resource
    private CheckgroupCheckitemMapper checkgroupCheckitemMapper;

    @Override
    public PageResult getCheckItems(QueryPageBean queryPageBean) {
        Page<Checkitem> page = new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        QueryWrapper<Checkitem> queryWrapper =  new QueryWrapper<>();
        if (StringUtils.isNotBlank(queryPageBean.getQueryString())){
            queryWrapper.like("code",queryPageBean.getQueryString())
                    .or().like("name",queryPageBean.getQueryString());
        }
        Page<Checkitem> checkitemPage = checkitemMapper.selectPage(page, queryWrapper);
        PageResult pageResult = new PageResult(checkitemPage.getTotal(),checkitemPage.getRecords());
        return pageResult;
    }

    @Override
    public Result addCheckItem(Checkitem checkitem) {
        int rows = checkitemMapper.insert(checkitem);
        if (rows==1){
            return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
        }
        return new Result(true, MessageConstant.ADD_CHECKITEM_FAIL);
    }
    @Override
    public Result updateCheckItem(Checkitem checkitem) {
        int rows = checkitemMapper.updateById(checkitem);
        if (rows==1){
            return new Result(true, MessageConstant.EDIT_CHECKITEM_SUCCESS);
        }
        return new Result(true, MessageConstant.EDIT_CHECKITEM_FAIL);
    }

    @Override
    public Result deleteCheckItem(int id) {
        //先删除中间表中的数据
        QueryWrapper<CheckgroupCheckitem> queryWrapper = new QueryWrapper();
        queryWrapper.eq("checkitem_id",id);
        checkgroupCheckitemMapper.delete(queryWrapper);
        //删除检查项
        int rows = checkitemMapper.deleteById(id);
        if (rows==1){
            return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);
        }
        return new Result(true, MessageConstant.DELETE_CHECKITEM_FAIL);

    }

    @Override
    public Result getAllCheckItem() {
        List<Checkitem> checkitemList=null;
        try {
            checkitemList = checkitemMapper.selectList(null);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"获取检查项失败");
        }
        return new Result(true,"获取检查项成功",checkitemList);
    }
}
