package com.offcn.service.impl;

import com.offcn.mapper.RoleMapper;
import com.offcn.pojo.Role;
import com.offcn.service.RoleService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleMapper roleMapper;

    @Override
    public List<Role> listByRoleId(int id) {
        return roleMapper.listByRoleId(id);
    }
}
