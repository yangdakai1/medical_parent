package com.offcn.service.impl;

import com.offcn.mapper.MenuMapper;
import com.offcn.pojo.Menu;
import com.offcn.service.MenuService;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuMapper menuMapper;

    @Override
    public List<Menu> getMenuById(int id) {
        return menuMapper.getMenuById(id);
    }
}
