package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.MemberMapper;
import com.offcn.mapper.OrderMapper;
import com.offcn.mapper.SetmealMapper;
import com.offcn.pojo.Member;
import com.offcn.pojo.Order;
import com.offcn.pojo.Setmeal;
import com.offcn.service.OrderService;
import com.offcn.utils.DateUtils;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import com.offcn.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;


    @Override
    public int getTodayOrderNumber() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("orderDate",today);
            return orderMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getTodayVisitsNumber() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("orderDate",today).eq("orderStatus","已到诊");
            return orderMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getThisWeekOrderNumber() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            //获取周一
            String monday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.between("orderDate",monday,today);
            return orderMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getThisWeekVisitsNumber() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            //获取周一
            String monday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.between("orderDate",monday,today).eq("orderStatus","已到诊");
            return orderMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getThisMonthOrderNumber() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            //获取周一
            String month = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.between("orderDate",month,today);
            return orderMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getThisMonthVisitsNumber() {
        try {
            String today = DateUtils.parseDate2String(new Date());
            //获取周一
            String month = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.between("orderDate",month,today).eq("orderStatus","已到诊");
            return orderMapper.selectCount(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Resource
    private MemberMapper memberMapper;
    @Resource
    private SetmealMapper setmealMapper;

    @Override
    public PageResult queryAllOrder(QueryPageBean queryPageBean) {
        Page<Order> page = new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(queryPageBean.getQueryString())){
            queryWrapper.like("id",queryPageBean.getQueryString());
        }
        Page<Order> orderPage = orderMapper.selectPage(page, queryWrapper);
        List<Order> orderList = orderPage.getRecords();
        for (Order order : orderList) {
            Member member = memberMapper.selectById(order.getMemberId());
            Setmeal setmeal = setmealMapper.selectById(order.getSetmealId());
            order.setMember(member);
            order.setSetmeal(setmeal);
        }
        PageResult pageResult = new PageResult(orderPage.getTotal(), orderList);
        return pageResult;
    }

    @Override
    public Result deleteOrder(int id) {
        int row = orderMapper.deleteById(id);
        if (row==1){
            return new Result(true, "删除预约成功");
        }
        return new Result(false,"删除预约失败");
    }

    @Override
    public Result updateOrder(Order order) {
        try {
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id",order.getId());
            Order orderOne = orderMapper.selectOne(queryWrapper);

            Integer memberId = orderOne.getMemberId();
            Integer setmealId = orderOne.getSetmealId();

            Member member = memberMapper.selectById(memberId);
            Setmeal setmeal = setmealMapper.selectById(setmealId);

            memberMapper.updateById(member);
            setmealMapper.updateById(setmeal);

            orderMapper.updateById(order);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"修改预约失败");
        }
        return new Result(true,"修改预约成功");
    }
}
