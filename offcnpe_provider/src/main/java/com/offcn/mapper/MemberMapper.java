package com.offcn.mapper;

import com.offcn.pojo.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.offcn.pojo.MemberEchartsVo;
import com.offcn.pojo.SetmealEcharsVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
public interface MemberMapper extends BaseMapper<Member> {
    List<MemberEchartsVo> countMemberBySex();

    List<MemberEchartsVo> countMemberByRegTime();
}
