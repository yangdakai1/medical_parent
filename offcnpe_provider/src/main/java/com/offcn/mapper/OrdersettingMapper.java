package com.offcn.mapper;

import com.offcn.pojo.Ordersetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.offcn.utils.PageResult;
import com.offcn.utils.QueryPageBean;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
public interface OrdersettingMapper extends BaseMapper<Ordersetting> {
    public PageResult listTodayOrder(QueryPageBean queryPageBean);

}
