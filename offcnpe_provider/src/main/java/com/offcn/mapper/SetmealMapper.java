package com.offcn.mapper;

import com.offcn.pojo.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.offcn.pojo.SetmealEcharsVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {
    List<SetmealEcharsVo> countSetmeal();
    List<Map<String,Object>> listHotSetmeal();

}
