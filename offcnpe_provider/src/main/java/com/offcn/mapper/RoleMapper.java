package com.offcn.mapper;

import com.offcn.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
public interface RoleMapper extends BaseMapper<Role> {
    List<Role> listByRoleId(int id);
}
