package com.offcn.mapper;

import com.offcn.pojo.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
public interface MenuMapper extends BaseMapper<Menu> {
    List<Menu> getMenuById(int id);
}
