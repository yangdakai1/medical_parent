package com.offcn.mapper;

import com.offcn.pojo.SetmealCheckgroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2021-11-01
 */
public interface SetmealCheckgroupMapper extends BaseMapper<SetmealCheckgroup> {

}
