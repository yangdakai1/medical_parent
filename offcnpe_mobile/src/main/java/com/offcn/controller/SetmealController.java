package com.offcn.controller;

import com.offcn.pojo.Member;
import com.offcn.pojo.Setmeal;
import com.offcn.pojo.SetmealAndCheckgroup;
import com.offcn.service.SetmealService;
import com.offcn.utils.MessageConstant;
import com.offcn.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Reference
    private SetmealService setmealService;

    @RequestMapping
    public Result list() {
        try {
            List<Setmeal> setmealList = setmealService.quertListSetmeal();
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmealList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }

    @GetMapping("/getById/{id}")
    public Result getById(@PathVariable("id") int id){
        try {
            Setmeal setmeal = setmealService.getSetmealById(id);
            return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,setmeal);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }

    @GetMapping("/getByInfo/{id}")
    public Result getByInfo(@PathVariable("id") int id){
        try {
            Setmeal setmeal = setmealService.getInfoById(id);
            return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,setmeal);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }

    @PostMapping("/setOrder")
    public Result Order(@RequestBody  Map<String,String> map){
        try {
            return setmealService.setOrder(map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"预约失败,请稍后再试！");
        }
    }

}
