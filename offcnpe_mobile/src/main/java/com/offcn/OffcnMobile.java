package com.offcn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import javax.swing.*;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class OffcnMobile {
    public static void main(String[] args) {
        SpringApplication.run(OffcnMobile.class,args);
    }
}
